const { FighterRepository } = require('../repositories/fighterRepository');
const { fighter } = require('../models/fighter');

class FighterService {

    getAllFighters() {
        return FighterRepository.getAll();
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    createFighter(inputData) {
        const { name, health, power, defense } = inputData;

        let fighterEntity = fighter;
        fighterEntity.name = name;
        fighterEntity.health = health;
        fighterEntity.power = power;
        fighterEntity.defense = defense;

        return FighterRepository.create(fighterEntity);
    }

    updateFighter(id, newData) {
        const { name, health, power, defense } = newData;

        let newFighterData = {
            name: name,
            health: health,
            power: power,
            defense: defense
        }
        return FighterRepository.update(id, newFighterData);
    }

    deleteFighter(id) {
        FighterRepository.delete(id);
    }
}

module.exports = new FighterService();