const { UserRepository } = require('../repositories/userRepository');
const { user } = require('../models/user');

class UserService {

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAllUsers() {
        return UserRepository.getAll();
    }

    createUser(userData) {
        const { firstName, lastName, email, phoneNumber, password } = userData;

        let userEntity = user;
        userEntity.firstName = firstName;
        userEntity.lastName = lastName;
        userEntity.email = email;
        userEntity.phoneNumber = phoneNumber;
        userEntity.password = password;

        return UserRepository.create(userEntity);
    }

    updateUser(id, newData) {
        const { firstName, lastName, email, phoneNumber, password } = newData;

        let newUserData = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phoneNumber: phoneNumber,
            password: password
        }
        return UserRepository.update(id, newUserData);
    }

    deleteUser(id) {
        UserRepository.delete(id);
    }
}

module.exports = new UserService();