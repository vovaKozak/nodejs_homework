const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    let resultValidate = validate(req);
    if (resultValidate.error) {
        return res.status(400).json(resultValidate);
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    let resultValidate = validate(req);
    if (resultValidate.error) {
        return res.status(400).json(resultValidate);
    }

    next();
}

function validate(req) {
    const { power } = req.body;

    let isError = false;
    let messageText = '';

    let requiredFields = checkRequiredFields(req.body);
    if (requiredFields.error) {
        isError = true;
        messageText = requiredFields.message;
    }

    if (typeof parseInt(power) != 'number' || power > 100) {
        isError = true;
        messageText += ' Power should be number and <= 100';
    }

    return { 'error': isError, 'message': messageText}
}

function checkRequiredFields(params) {
    const { id } = params;

    let isError = false;
    let errorMessage = '';

    if (id) {
        isError = true;
        errorMessage = 'Id should not be in body! ';
    }

    Object.keys(fighter).forEach(function (propertyName) {
        if (propertyName != 'id') {
            if (!Object.keys(params).includes(propertyName)) {
                isError = true;
                errorMessage += propertyName + ' is required! ';
            }
        }
    });

    return {'error': isError, 'message': errorMessage};
}


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;