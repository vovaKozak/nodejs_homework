const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    let resultValidate = validate(req);
    if (resultValidate.error) {
        return res.status(400).json(resultValidate);
    }

    next();
}

const updateUserValid = (req, res, next) => {

    let resultValidate = validate(req);
    if (resultValidate.error) {
        return res.status(400).json(resultValidate);
    }

    next();
}

function validate(req) {

    let isError = false;
    let messageText = '';

    const { email, phoneNumber, password } = req.body;

    let requiredFields = checkRequiredFields(req.body);
    if (requiredFields.error) {
        isError = true;
        messageText = requiredFields.message;
    }

    if (!validateEmail(email)) {
        isError = true;
        messageText += ' Email is not valid';
    }

    if (!validatePhone(phoneNumber)) {
        isError = true;
        messageText += ' Phone is not in +380xxxxxxxxx format';
    }

    if (password.length < 3) {
        isError = true;
        messageText += ' Password is min 3 symbols!';
    }

    return { 'error': isError, 'message': messageText};
}

function checkRequiredFields(params) {
    const { id } = params;

    let isError = false;
    let errorMessage = '';

    if (id) {
        isError = true;
        errorMessage = 'Id should not be in body! ';
    }

    Object.keys(user).forEach(function (propertyName) {
        if (propertyName != 'id') {
            if (!Object.keys(params).includes(propertyName)) {
                isError = true;
                errorMessage += propertyName + ' is required! ';
            }
        }
    });

    return {'error': isError, 'message': errorMessage};
}

const validatePhone = (phone) => {
    const phoneIsValid = phone &&
        phone.length == 13 &&
        phone.substring(0, 4) == '+380' &&
        /^\d{9}$/.test(phone.substring(4))
    ;
    return phoneIsValid;
}

const validateEmail = (email) => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(email) && email.substring(email.length - 10, email.length) === '@gmail.com';
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;