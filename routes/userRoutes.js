const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// get All users
router.get('/', function (req, res) {
    const users = UserService.getAllUsers();

    res.json(users);
});

// get One user by id
router.get('/:id', function (req, res) {
    const userId = req.params.id;
    const user = UserService.search({ id: userId });

    if (user) {
        res.status(200).json(user);
    } else {
        res.status(404).json({'error': true, 'message': 'User not found'});
    }
});

// create New user
router.post('/', [createUserValid, function (req, res) {
    let user = UserService.createUser(req.body);

    res.status(200).json(user);
}]);

// update User
router.put('/:id', [updateUserValid, function (req, res) {
    const userId = req.params.id;
    let user = UserService.updateUser(userId, req.body);

    if (user) {
        res.status(200).json(user);
    } else {
        res.status(404).json({'error': true, 'message': 'User not found'});
    }
}]);

// delete User
router.delete('/:id', function (req, res) {
    const userId = req.params.id;
    UserService.deleteUser(userId);

    res.status(200).json({status: 'success'});
});

module.exports = router;