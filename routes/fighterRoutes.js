const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// get All fighters
router.get('/', function (req, res) {
    const users = FighterService.getAllFighters();

    res.json(users);
});

// get One fighter
router.get('/:id', function (req, res) {
    const fighterId = req.params.id;
    const fighter = FighterService.search({ id: fighterId });

    if (fighter) {
        res.status(200).json(fighter);
    } else {
        res.status(404).json({'error': true, 'message': 'Fighter not found'});
    }
});


// create Fighter
router.post('/', [createFighterValid, function (req, res) {
    let fighter = FighterService.createFighter(req.body);

    res.status(200).json(fighter);
}]);

// update Fighter
router.put('/:id', [updateFighterValid, function (req, res) {
    const fighterId = req.params.id;
    let fighter = FighterService.updateFighter(fighterId, req.body);

    if (fighter) {
        res.status(200).json(fighter);
    } else {
        res.status(404).json({'error': true, 'message': 'Fighter not found'});
    }
}]);

// delete Fighter
router.delete('/:id', function (req, res) {
    const fighterId = req.params.id;
    FighterService.deleteFighter(fighterId);

    res.status(200).json({status: 'success'});
});

module.exports = router;